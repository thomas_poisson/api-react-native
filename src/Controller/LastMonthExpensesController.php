<?php

namespace App\Controller;
use App\Repository\ExpenseRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LastMonthExpensesController extends AbstractController
{
    #[Route('/api/user/{id}/lastmonth', name: 'last_month_expends')]
    public function LastMonthExpenses($id, ExpenseRepository $repo, SerializerInterface $serializer): Response
    {

        $expenses = $repo->findLastMonth($id);
        
        $get = $serializer->serialize($expenses, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }
}
